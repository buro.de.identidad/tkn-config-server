package com.teknei.bid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class TknConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TknConfigServerApplication.class, args);
	}
}
