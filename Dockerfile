FROM openjdk:8-jre-alpine
COPY target/tkn-config-server-1.0.0.jar /home/tkn-config-server.jar
COPY script_init.sh /home/script_init.sh
RUN chmod 777 /home/script_init.sh
RUN mkdir -p /opt/bidserver/logs
COPY tkn-config-repo.tar.gz /home/tkn-config-repo.tar.gz
RUN cd /home
RUN tar -xzvf /home/tkn-config-repo.tar.gz -C /home
EXPOSE 8001
ENTRYPOINT ["/home/script_init.sh"]
